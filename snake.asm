	IDEAL
	MODEL small
	STACK 100h
	DATASEG
	; --------------------------
	; Your variables here
	Clock equ es:6Ch
	key dw 0
    key1 db 'a'
	dir db '0'
	num_of_stars dw 7
	head dw (80*12 +38)*2,(80*12 +39)*2,(80*12 +40)*2,(80*12 +41)*2,,(80*12 +42)*2,(80*12 +43)*2,(80*12 +44)*2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 ;מיקום 0,1
	; --------------------------
	CODESEG
	proc clean
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;cleans the screan
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	push bp			;pushes
	mov bp,sp
	push ax
	push es
	push bx
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		xor ax,ax	;reset ax
		mov al,' '	;mvo al nothing to make black
		xor bx,bx	;reset bx
		looop:		;draw plack on the screen until (25*80*2) place
		mov [es:bx],ax
		add bx,2
		cmp bx,25*80*2
		jnz looop
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	pop bx			;pops
	pop es
	pop ax
	pop bp
	ret
	endp clean
		;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc star
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; go to ds, takes the places and draw stars at those places
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	push bp				;pushes
	mov bp,sp
	push bx
	push cx
	push si
	push ax
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		mov al,'*'		;mov al thw sighn of the star
		mov ah,0bh		;mov ah the color of the star
		xor cx,cx		;reset cx

	lop_star:			;put star in every place we have from the dt
		mov bx,[bp+4]	; mov bx the place of [head] in dt
		add bx,cx		; add bx the num of rounds we completed,example: if we completed one round we wll add bx 2(dw) to go to next place of [head] in dt
		mov bx,[bx]		; change bx to the value we have in [head]
		mov [es:bx],ax	;draw
		add cx,2		
		mov si,[bp+6]	;mov si the plae of the num of stars we have at first
		mov si,[si]		;mov si the value we have in it
		sal si,1		; double the value
		cmp cx,si		; check if we already draw the last star
		jnz lop_star
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;s
	pop ax
	pop si
	pop cx
	pop bx
	pop bp
		ret 
		endp star
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;make the snake move slower
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc delay
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla:
	inc ax
	cmp ax,650
	jnz bla
	xor ax,ax
	inc bx
	cmp bx,110
	jnz bla
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay

proc spawn_apple
;the proceudre generates a random apple on the screen and savs the locaiton in apple_location in the data segment 
;----------pushes-----------------
    push bp
    mov bp,sp
    push ax
    push bx
    push di
    push es
;--------------------------------
    mov ax,40h
    mov es,ax                   ;moves es to take bits from the clock
    mov di,[bp+4]
    loprandom:
    mov bx,[es:006ch]
    xor bh,bl
    and bx,0000011111111111b
    cmp bx,1999
    jg loprandom
    sal bx,1
    mov [di],bx
    put_apple:
    mov al,'&'
    mov ah,0Dh
;-------end pops----------------------
    pop es
    pop di
    mov [es:bx],ax    ;puts apple on screen
    pop bx
    pop ax
    pop bp
    ret 
endp spawn_apple
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc check_apple
;the procedure receives the apple location offset head
;the procedure checks if the snake has hit the apple. if it does, it adds another star to the snake
;----pushes------------------
    push bp
    mov bp,sp
    push ax
    push bx
    push dx
    push di
;----------------------------
    mov bx,[bp+8]
    mov ax,[bx]   ;location of apple
    mov bx,[bp+4]
    mov dx,[bx]     ;location of head
    cmp ax,dx
    jnz end_check_apple
    mov di,[bp+6]
    mov ax,[di]
    dec ax
    sal ax,1
    add bx,ax   ;bx gets adress of last star
    mov dx,[bx]
    add bx,2
    mov [bx],dx
    inc [word ptr di]
    mov di,[bp+8]
;-------push offset apple_location for next procedure-----------
    push di
    call spawn_apple
    pop di
;-------end pops-------------------
    end_check_apple:
    pop di
    pop dx
    pop bx
    pop ax
    pop bp
    ret 
endp check_apple
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc delete_last_star
;----pushes------------------
	push bp
	mov bp,sp
	push ax
	push si
	push bx
	push di
;----pushes------------------
		
		mov al, ' '			;put in ax the black display
		mov ah, 0
		mov si,[bp+6]	
		mov si,[si]			;set in si the cuurent num of dw the snake uses in ds
		sal si,1			
		sub si,2
		mov bx,[bp+4]
		add bx,si			; adding to bx the dw*2-2 places in dx (bx gets the last star location)
		mov bx,[bx]
		mov [es:bx],ax		;draw
;-------end pops----------------------
	pop di
	pop bx
	pop si
	pop ax
	pop bp
	ret
		endp delete_last_star

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;mov the head of the snake one line down
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc move_head_down
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push di
	push ax
;----pushes------------------
		mov al,'*'		;mov al thw sighn of the star
		mov ah,0bh		;mov ah the color of the star
	mov bx,[bp+4]
	mov bx,[bx]
		add bx,160			;mov the head of the snake one line down
		mov ah,0bh			;color
		mov di,[bp+4]
		mov [di],bx			;change in ds the head's place
		mov [es:bx],ax		;draw
;-------end pops----------------------
	pop ax
	pop di
	pop bx
	pop bp
	ret
	endp move_head_down
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;mov the head of the snake one line up
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc move_head_up
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push di
	push ax
;----pushes------------------
		mov al,'*'		;mov al thw sighn of the star
		mov ah,0bh		;mov ah the color of the star
	mov bx,[bp+4]
	mov bx,[bx]
		sub bx,160		;mov the head of the snake one line up
		mov ah,0bh		;color
		mov di,[bp+4]
		mov [di],bx		;change in ds the head's place
		mov [es:bx],ax	;draw
		
;-------end pops----------------------
	pop ax
	pop di
	pop bx
	pop bp
	ret
	endp move_head_up
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;mov the head of the snake one place right
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc move_head_right
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push di
	push ax
;----pushes------------------
		mov al,'*'		;mov al thw sighn of the star
		mov ah,0bh		;mov ah the color of the star
	mov bx,[bp+4]
	mov bx,[bx]
		add bx,2		;mov the head of the snake one place right
		mov ah,0bh		;color
		mov di,[bp+4]
		mov [di],bx		;change in ds the head's place
		mov [es:bx],ax	;draw
		
;-------end pops----------------------
	pop ax
	pop di
	pop bx
	pop bp
	ret
	endp move_head_right

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;mov the head of the snake one lace left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc move_head_left
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push di
	push ax
;----pushes------------------
		mov al,'*'		;mov al thw sighn of the star
		mov ah,0bh		;mov ah the color of the star
	mov bx,[bp+4]
	mov bx,[bx]
		sub bx,2		;mov the head of the snake one lace left
		mov ah,0bh		;color
		mov di,[bp+4]
		mov [di],bx		;change in ds the head's place
		mov [es:bx],ax	;draw
		
;-------end pops----------------------
	pop ax
	pop di
	pop bx
	pop bp
	ret
	endp move_head_left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;move the body of the snkae
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc move_all_kinds
;----pushes------------------
		push bp
		mov bp,sp
		push bx
		push ax
		push cx
		push si
		push di
;----pushes------------------
		mov si,[bp+6]
		mov si,[si]
		sal si,1
		sub si,2		;set si the num of dw*2-2= num of db-2
	every:
		mov al,'*'
		mov ah,0bh
		mov bx,[bp+4]
		add bx,si				; mov to bx the last star
		sub si,2
		mov di,[bp+4]
		add di,si				; mov to di the last-1 star
		mov di,[word ptr di]
		mov [bx],di				; mov the star to the star-1
		cmp si,0
		jnz every
;-------end pops----------------------
		pop di
		pop si
		pop cx
		pop ax
		pop bx
		pop bp
		ret 
		endp move_all_kinds
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;checks if head is on the border
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc borders_up
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push si
	push dx
;----pushes------------------
	mov dx,0
	mov si,[bp+6]
	mov si,[si]
	cmp si,160	
	jl sonis2	;checks if head is in the top border
	mov dx,1	;if no put in dx 1 if no
	
	sonis2:
	mov [bp+4], dx
;-------end pops----------------------
	pop di
	pop si
	pop dx
	pop bx
	pop bp
	ret
	endp borders_up
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;checks if head is on the border
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc borders_right
;----pushes------------------
	push bpz
	mov bp,sp
	push bx
	push si
	push dx
	push ax
;----pushes------------------
	xor dx,dx
	mov si,[bp+6]
	mov si,[si]
	mov ax,si
	mov bx,159
	div bx		;checks if head is in the right bordet
	mov [bp+4], dx	;mov the rest to dx, of head is on the border so rest=0
;-------end pops----------------------
	pop di
	pop si
	pop dx
	pop ax
	pop bx
	pop bp
	ret
	endp borders_right
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;checks if head is on the border
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc borders_left
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push si
	push dx
	push ax
;----pushes------------------
	xor dx,dx
	mov si,[bp+6]
	mov si,[si]
	mov ax,si
	mov bx,160
	div bx		;checks if head is in the left bordet			
	mov [bp+4], dx 	;mov the rest to dx, of head is on the border so rest=0
;-------end pops----------------------
	pop di
	pop si
	pop dx
	pop ax
	pop bx
	pop bp
	ret
	endp borders_left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;checks if head is on the border
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc borders_down
;----pushes------------------
	push bp
	mov bp,sp
	push bx
	push si
	push dx
;----pushes------------------
	mov dx,0
	mov si,[bp+6]
	mov si,[si]
	cmp si,160*24
	ja sonis 	;checks if head is in the low border
	mov dx,1 	
	
	sonis:
	mov [bp+4], dx	;if no put in dx 1 if no
;-------end pops----------------------
	pop di
	pop si
	pop dx
	pop bx
	pop bp
	ret
	endp borders_down
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc check_snake_left
    push bp
    mov bp,sp
    push ax
    push bx
;;;;;;;;;;;;;;;;;;
    mov bx,[bp+4]    ;bx gets the offset of head
    mov bx,[bx]
    mov ax,[es:bx-2]
    mov [word ptr bp+6],1
    cmp al,'*'
    jnz end_check_snake_left     ;if there is nothing in the right, [bp+4]=1
    mov [word ptr bp+6],0
;;;;;;;;;;;;;;;;;
    end_check_snake_left:
    pop bx
    pop ax
    pop bp
    ret 2
    endp check_snake_left
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc right_procs
;----pushes--------------------------
	push bp
	mov bp,sp
		push [bp+8]
		push [bp+6]
		push [bp+4]
;----pushes--------------------------
		call delete_last_star
		call move_all_kinds
		call move_head_right
		call delay
		call star
		call check_apple
;-------end pops----------------------
	pop[bp+4]
	pop[bp+6]
	pop[bp+8]
	pop bp
	ret 
	endp right_procs
	
	proc left_procs
;----pushes--------------------------
	push bp
	mov bp,sp
		push [bp+8]
		push [bp+6]
		push [bp+4]
;----pushes--------------------------
		call delete_last_star
		call move_all_kinds
		call move_head_left
		call delay
		call star
		call check_apple
;-------end pops----------------------
	pop[bp+4]
	pop[bp+6]
	pop[bp+8]
	pop bp
	ret 
	endp left_procs
	
	proc up_procs
;----pushes--------------------------
	push bp
	mov bp,sp
	push [bp+8]
		push [bp+6]
		push [bp+4]
;----pushes--------------------------
		call delete_last_star
		call move_all_kinds
		call move_head_up
		call delay
		call star
		call check_apple
;-------end pops----------------------
	pop[bp+4]
	pop[bp+6]
	pop[bp+8]
	pop bp
	ret 
	endp up_procs
	
	proc down_procs
;----pushes--------------------------
	push bp
	mov bp,sp
	push [bp+8]
	push [bp+6]
	push [bp+4]
;----pushes--------------------------
		call delete_last_star
		call move_all_kinds
		call move_head_down
		call delay
		call star
		call check_apple
;-------end pops----------------------
	pop[bp+4]
	pop[bp+6]
	pop[bp+8]
	pop bp
	ret 
	endp down_procs
	start:
		mov ax, @data
		mov ds, ax
	; --------------------------
	; Your code here
		mov ax,0b800h
		mov es,ax
		mov cx,160
		call clean;clear
		push offset key
		
		call spawn_apple
		push offset key
		push offset num_of_stars
		push offset head
		call star ;* in the middle
		
		lop:
		push offset key
		push offset num_of_stars
		push offset head
		mov ah,0
		int 16h
		mov [dir],al
		cmp [dir],'a'
		jz left1
		cmp [dir],'w'
		jz up1
		cmp [dir],'d'
		jz right1
		cmp [dir],'s'
		jz down1
		jmp lop
		
	left1:
		cmp [key1],'d'
		jz right1
		mov al,'a'
		mov [key1],al
		push dx
		call borders_left
		pop dx
		cmp dx,0
		jz exit
		call left_procs
	mov ah,1
	int 16h
	jz left1
		jmp lop
		
	up1:
		cmp [key1],'s'
		jz down1
		mov al,'w'
		mov [key1],al
		push dx
		call borders_up
		pop dx
		cmp dx,0
		jz exit
		call up_procs
	mov ah,1
	int 16h
	jz up1
		jmp lop
	right1:																														
		cmp [key1],'a'
		jz left1
		mov al,'d'
		mov [key1],al
		push dx
		call borders_right
		pop dx
		cmp dx,0
		jz exit
		call right_procs
	mov ah,1
	int 16h
	jz right1
		jmp lop
		
		down1:
		cmp [key1],'w'
		jz up1
		mov al,'s'
		mov [key1],al
		push dx
		call borders_down
		pop dx
		cmp dx,0
		jz exit
		call down_procs
	mov ah,1
	int 16h
	jz down1
		jmp lop
	
	; --------------------------
		
	exit:
		mov ax, 4c00h
		int 21h
	END start
	
	


