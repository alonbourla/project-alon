IDEAL
MODEL small
STACK 100h
DATASEG
; --------------------------
; Your variables here
dinosaur_body dw ((194*320)+20)
triangle_body dw ((320*191)+260)
ribua_body dw ((320*191)+390)

; --------------------------
CODESEG
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג הגדרות מסך;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc under_line
push bp
mov bp,sp
push bx
push ax
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
mov bx,320*199
lop2:
mov ax,04eh
mov [es:bx],ax
inc bx
cmp bx,64001
jnz lop2
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
pop ax
pop bx
pop bp
ret
endp under_line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc clear
push bp
mov bp,sp
push bx
push ax
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
mov bx,0
mov ax,0
lop1:

mov [es:bx],ax
inc bx
cmp bx,64001
jnz lop1

;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
pop ax
pop bx
pop bp
ret
endp clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc delay
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla:
	inc ax
	cmp ax,900
	jnz bla
	xor ax,ax
	inc bx
	cmp bx,450
	jnz bla
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc delay_just_slower
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla2:
	inc ax
	cmp ax,70
	jnz bla2
	xor ax,ax
	inc bx
	cmp bx,500
	jnz bla2
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay_just_slower
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

proc delay_just_slower2
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla3:
	inc ax
	cmp ax,30
	jnz bla3
	xor ax,ax
	inc bx
	cmp bx,500
	jnz bla3
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay_just_slower2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  פאקינג נגמר הגדרות מסך;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג דינוזאור;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur
push bp				;pushes
	mov bp,sp
	push bx
	push cx
	push si
	push ax
	push di
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mov ax,7
	xor cx,cx
	xor si,si
	mov bx,[bp+4]
	mov bx,[bx]
	
	dinosaur_lop1:
	mov [es:bx],ax
	inc bx
	inc cx
	cmp cx,5
	jnz dinosaur_lop1
	mov di,0
	inc si
	mov bx,[bp+4]
	mov bx,[bx]
	mov cx,0
	dinosaur_lop2:
	inc di
	add bx,320
	cmp di,si
	jnz dinosaur_lop2
	cmp si,5
	jnz dinosaur_lop1
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	pop di
	pop ax
	pop si
	pop cx
	pop bx
	pop bp
	ret 2
	endp dinosaur
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur_clear
push bp				;pushes
mov bp,sp
push bx
push cx
push si
push ax
push di
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mov ax,255
xor cx,cx
xor si,si
mov bx,[bp+4]
mov bx,[bx]

dinosaur_clear_lop1:
mov [es:bx],ax
inc bx
inc cx
cmp cx,5
jnz dinosaur_lop1
mov di,0
inc si
mov bx,[bp+4]
mov bx,[bx]
mov cx,0
dinosaur_clear_lop2:
inc di
add bx,320
cmp di,si
jnz dinosaur_clear_lop2
cmp si,5
jnz dinosaur_clear_lop1
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pop di
pop ax
pop si
pop cx
pop bx
pop bp	
ret 2
endp dinosaur_clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc check_jump_up
push bp
mov bp,sp
push bx
push si
;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov si,bx
mov si,[si]
sub si,320
mov [bx],si
;;;;;;;;;;;;;;;
pop si
pop bx
pop bp
ret 2
endp check_jump_up
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;:::::::::
proc check_jump_down
push bp
mov bp,sp
push bx
push si
;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov si,bx
mov si,[si]
add si,320
mov [bx],si
;;;;;;;;;;;;;;;
pop si
pop bx
pop bp
ret 2
endp check_jump_down
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג נגמר הדינוזאור;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג משולש;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle_clear
push bp
mov bp,sp
push bx
push ax
push cx

;;;;;;;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov bx,[bx]
mov ax,255
xor cx,cx
triangle_clear_lop1:
mov [es:bx],ax
add bx,319
inc cx
cmp cx,8
jnz triangle_clear_lop1
mov bx,[bp+4]
mov bx,[bx]
xor cx,cx
triangle_clear_lop2:
mov [es:bx],ax
add bx,321
inc cx
cmp cx,8
jnz triangle_clear_lop2
;;;;;;;;;;;;;;;;;;;;;;;;;
pop cx
pop ax
pop bx
pop bp
ret 2
endp triangle_clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle_procs
push bp
mov bp,sp
push [bp+4]
call triangle_clear
push [bp+4]
call move_triangle
push [bp+4]
call triangle
call delay_just_slower
pop bp
ret 2
endp triangle_procs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle
push bp
mov bp,sp
push bx
push ax
push cx

;;;;;;;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov bx,[bx]
mov ax,7
xor cx,cx
triangle_lop1:
mov [es:bx],ax
add bx,319
inc cx
cmp cx,8
jnz triangle_lop1
mov bx,[bp+4]
mov bx,[bx]
xor cx,cx
triangle_lop2:
mov [es:bx],ax
add bx,321
inc cx
cmp cx,8
jnz triangle_lop2
;;;;;;;;;;;;;;;;;;;;;;;;;
pop cx
pop ax
pop bx
pop bp
ret 2
endp triangle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc move_triangle
push bp
mov bp,sp
push bx
push si
;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov si,bx
mov si,[si]
sub si,1
mov [bx],si
;;;;;;;;;;;;;;;
pop si
pop bx
pop bp
ret 2
endp move_triangle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג נגמר המשולש;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



proc obs_proc
push bp
mov bp,sp
push [bp+4]
call triangle_clear
push [bp+4]
call move_triangle
push [bp+4]
call triangle
pop bp
ret 2
endp obs_proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc obs_proc_2
push bp
mov bp,sp
push [bp+4]
call triangle_procs
push [bp+6]
call ribua_procs
pop bp
ret 4
endp obs_proc_2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur_procs
push bp
mov bp,sp
push ax
mov ax,0

jump_up_twice:
push [bp+8]
push [bp+6]
call obs_proc_2
push [bp+4]
call dinosaur_clear
inc ax
push [bp+4]
call check_jump_up
push [bp+4]
call dinosaur
call delay_just_slower
push [bp+4]
call dinosaur_clear
cmp ax,30
jnz jump_up_twice
push [bp+4]
call dinosaur
mov ax,0
push [bp+8]
push [bp+6]
call obs_proc_2
jump_down_twice:
inc ax
push [bp+8]
push [bp+6]
call obs_proc_2
push [bp+4]
call dinosaur_clear
push [bp+4]
call check_jump_down
push [bp+4]
call dinosaur
call delay_just_slower
cmp ax,30
jnz jump_down_twice
push [bp+4]
call dinosaur
pop ax
pop bp
ret 6
endp dinosaur_procs
start:
	mov ax, @data
	mov ds, ax
; --------------------------
; Your code here
mov ax,13h
int 10h
mov ax,0a000h
mov es, ax
call clear
call under_line
push offset dinosaur_body
call dinosaur
push offset triangle_body
call triangle
push offset triangle_body
call move_triangle
push offset triangle_body
call triangle
main_lop:
	
	mov ah,0
	int 16h
	cmp al,'w'
	jz dinosaur_jump
	jmp main_lop
	
dinosaur_jump:
push offset ribua_body
push offset triangle_body
push offset dinosaur_body
call dinosaur_procs

fuck:
mov ah,1
	int 16h
	jz obs
jmp main_lop

obs:
push offset ribua_body
push offset triangle_body
call obs_proc_2
jmp fuck
; --------------------------

exit:
	mov ax, 4c00h
	int 21h
END start


