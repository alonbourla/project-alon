	IDEAL
	MODEL small
	STACK 100h
	DATASEG
	; --------------------------
	; Your variables here
	arraystart db 3,6,-78,90,-111,32,45,2,120,-76
	arrayend db 54
	; --------------------------
	CODESEG
	proc min
	push bp
	mov bp,sp
	push ax
	push di
	push bx
	push cx
	push si
	;;;;;;;;;;;;;;;;;;;;;;;
		mov di,[bp+6]		;place of  highest to di
		mov ax,di
		mov bx,[bp+4]		;place of lowest to bx
		add bx,si
		inc si
		mov [bp-10],si
		sub ax,bx			;num of db
		mov [bp-2],ax		;keep ax value
		inc bx			;
		mov di,bx		;di gets bx+1 place
		dec bx			;
		
	lop1:
		cmp ax,0		;ax times lop1
		jz finish
		mov cl,[byte ptr di]		;
		cmp [byte ptr bx],cl		;			if ds-di is smaller than ds-dx jump to lop3 lable 
		jg lop3						; 
	lop2:
		sub ax,1
		inc di
		jmp lop1
		
	lop3:							; move the small vlaue to bx from di
		sub ax,1
		mov bx,di
		inc di
		jmp lop1
		
	finish:	
	mov [bp-6],bx			;extract the data from stack seg to bx
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	pop si
	pop cx
	pop bx
	pop di
	pop ax
	pop bp

	ret
	endp min
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	proc swap
	push bp
	mov bp,sp
	push cx
	push di
	push bx
	;;;;;;;;;;;;;;;;;;;;;;;;;
		mov di,[bp+4]
		dec si
		add di,si
		inc si
		mov ch,[byte ptr di]
		mov cl,[byte ptr bx]
		mov [byte ptr di],cl
		mov [byte ptr bx],ch
	;;;;;;;;;;;;;;;;;;;;;;;;;
	pop bx
	pop di
	pop cx
	pop bp

	ret
	endp swap
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	start:
		mov ax, @data
		mov ds, ax
	; --------------------------
	; Your code here
		xor si,si
	again:

		push offset arrayend
		push offset arraystart
		call min
		; push offset arrayend
		; push offset arraystart
		call swap
		cmp ax,0
		jnz again
	; --------------------------
		
	exit:
		mov ax, 4c00h
		int 21h
	END start


