IDEAL
MODEL small
STACK 100h
DATASEG
; --------------------------
; Your variables here
dinosaur_body dw ((194*320)+20)
triangle_body dw ((320*191)+150)
ribua_body dw ((320*188)+270)
line_body dw ((320*188)+300)


; --------------------------
CODESEG
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג הגדרות מסך;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc under_line
push bp
mov bp,sp
push bx
push ax
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
mov bx,320*199
lop2:
mov al,7
mov [es:bx],al
inc bx
cmp bx,64001
jnz lop2
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
pop ax
pop bx
pop bp
ret
endp under_line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc clear
push bp
mov bp,sp
push bx
push ax
;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
mov bx,0
mov ax,0
lop1:

mov [es:bx],ax
inc bx
cmp bx,64001
jnz lop1

;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;
pop ax
pop bx
pop bp
ret
endp clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc delay
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla:
	inc ax
	cmp ax,400
	jnz bla
	xor ax,ax
	inc bx
	cmp bx,100
	jnz bla
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc delay_just_slower
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla2:
	inc ax
	cmp ax,60
	jnz bla2
	xor ax,ax
	inc bx
	cmp bx,500
	jnz bla2
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay_just_slower
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

proc delay_just_slower2
	push ax
	push bx
	mov ax,0
	xor bx,bx
	bla3:
	inc ax
	cmp ax,30
	jnz bla3
	xor ax,ax
	inc bx
	cmp bx,500
	jnz bla3
	;;;;;;;;;;;
	;;;;;;;;;;;
	pop bx
	pop ax
	ret
	endp delay_just_slower2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  פאקינג נגמר הגדרות מסך;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג דינוזאור;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur
push bp				;pushes
	mov bp,sp
	push bx
	push cx
	push si
	push ax
	push di
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mov ax,7
	xor cx,cx
	xor si,si
	mov bx,[bp+4]
	mov bx,[bx]
	
	dinosaur_lop1:
	mov [es:bx],ax
	inc bx
	inc cx
	cmp cx,5
	jnz dinosaur_lop1
	mov di,0
	inc si
	mov bx,[bp+4]
	mov bx,[bx]
	mov cx,0
	dinosaur_lop2:
	inc di
	add bx,320
	cmp di,si
	jnz dinosaur_lop2
	cmp si,5
	jnz dinosaur_lop1
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	pop di
	pop ax
	pop si
	pop cx
	pop bx
	pop bp
	ret 2
	endp dinosaur
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur_clear
push bp				;pushes
mov bp,sp
push bx
push cx
push si
push ax
push di
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mov ax,255
xor cx,cx
xor si,si
mov bx,[bp+4]
mov bx,[bx]

dinosaur_clear_lop1:
mov [es:bx],ax
inc bx
inc cx
cmp cx,5
jnz dinosaur_lop1
mov di,0
inc si
mov bx,[bp+4]
mov bx,[bx]
mov cx,0
dinosaur_clear_lop2:
inc di
add bx,320
cmp di,si
jnz dinosaur_clear_lop2
cmp si,5
jnz dinosaur_clear_lop1
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pop di
pop ax
pop si
pop cx
pop bx
pop bp	
ret 2
endp dinosaur_clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	proc check_jump_up
push bp
mov bp,sp
push bx
push si
;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov si,bx
mov si,[si]
sub si,320
mov [bx],si
;;;;;;;;;;;;;;;
pop si
pop bx
pop bp
ret 2
endp check_jump_up
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;:::::::::
proc check_jump_down
push bp
mov bp,sp
push bx
push si
;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov si,bx
mov si,[si]
add si,320
mov [bx],si

;;;;;;;;;;;;;;;
pop si
pop bx
pop bp
ret 2
endp check_jump_down
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc check_if_touch_triangle
push bp
mov bp,sp
push ax
push bx
push si
 mov bx,[bp+4]
 mov bx,[bx]
 add bx,5+1
 add bx,320*5
 mov ax,[es:bx]
 cmp ax,1
 pop si
 pop bx
 pop ax
 pop bp
 pop [bp+4] 
 endp check_if_touch_triangle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג נגמר הדינוזאור;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג משולש;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle_clear
push bp
mov bp,sp
push bx
push ax
push cx

;;;;;;;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov bx,[bx]
mov ax,255
xor cx,cx
triangle_clear_lop1:
mov [es:bx],ax
add bx,319
inc cx
cmp cx,8
jnz triangle_clear_lop1
mov bx,[bp+4]
mov bx,[bx]
xor cx,cx
triangle_clear_lop2:
mov [es:bx],ax
add bx,321
inc cx
cmp cx,8
jnz triangle_clear_lop2
;;;;;;;;;;;;;;;;;;;;;;;;;
pop cx
pop ax
pop bx
pop bp
ret 2
endp triangle_clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle_procs
push bp
mov bp,sp
push [bp+4]
call triangle_clear
push [bp+4]
call move_all_obs
push [bp+4]
call triangle
call delay_just_slower2
pop bp
ret 2
endp triangle_procs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc triangle
push bp
mov bp,sp
push bx
push ax
push cx

;;;;;;;;;;;;;;;;;;;;;;
mov bx,[bp+4]
mov bx,[bx]
mov ax,020h
xor cx,cx
triangle_lop1:
mov [es:bx],ax
add bx,319
inc cx
cmp cx,8
jnz triangle_lop1
mov bx,[bp+4]
mov bx,[bx]
xor cx,cx
triangle_lop2:
mov [es:bx],ax
add bx,321
inc cx
cmp cx,8
jnz triangle_lop2
;;;;;;;;;;;;;;;;;;;;;;;;;
pop cx
pop ax
pop bx
pop bp
ret 2
endp triangle
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc move_all_obs
push bp
mov bp,sp
push bx
push si
push ax
push dx
;;;;;;;;;;;;;;;;
mov si,[bp+4]
mov si,[si]
sub si,1

mov ax,si
mov bx,320
mov dx,0
div bx
cmp dx,0
jnz karin
mov ax,1
mov bx,[bp+4]
mov [bx],ax 
jmp kidan
karin:
mov bx,[bp+4]
mov [bx],si
kidan:
;;;;;;;;;;;;;;;
pop dx
pop ax
pop si
pop bx
pop bp
ret 2
endp move_all_obs

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג נגמר המשולש;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;פאקינג ריבוע;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc ribua
push bp				;pushes
	mov bp,sp
	push bx
	push cx
	push si
	push ax
	push di
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	mov ax,028h
	xor si,si
	mov bx,[bp+4]
	mov bx,[bx]
ribua_lop:
	mov [es:bx],ax
	inc bx
	inc si
	cmp si,20
	jnz ribua_lop
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pop di
pop ax
pop si
pop cx
pop bx
pop bp	
ret 2

endp ribua
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc ribua_clear
push bp				;pushes
mov bp,sp
push bx
push cx
push si
push ax
push di
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mov ax,255
	xor si,si
	mov bx,[bp+4]
	mov bx,[bx]
ribua_clear_lop:
	mov [es:bx],ax
	cmp si,30
	inc bx
	inc si
	jnz ribua_lop
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pop di
pop ax
pop si
pop cx
pop bx
pop bp	
ret 2
endp ribua_clear
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

proc ribua_procs
push bp
mov bp,sp
push [bp+4]
call ribua_clear
push [bp+4]
call move_all_obs
push [bp+4]
call ribua
call delay_just_slower2
pop bp
ret 2
endp ribua_procs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


proc obs_proc
push bp
mov bp,sp
push [bp+4]
call triangle_clear
push [bp+4]
call move_all_obs
push [bp+4]
call triangle
push [bp+6]
call ribua_clear
push [bp+6]
call move_all_obs
push [bp+6]
call ribua
pop bp
ret 4
endp obs_proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc obs_proc_2
push bp
mov bp,sp
push [bp+4]
call triangle_procs
push [bp+6]
call ribua_procs
pop bp
ret 4
endp obs_proc_2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
proc dinosaur_up_procs
push bp
mov bp,sp
push ax
push dx
push bx
mov ax,0

jump_up_twice:
push [bp+8]
push [bp+6]
call obs_proc
push [bp+4]
call dinosaur_clear
push [bp+4]
call check_jump_up
push [bp+4]
call dinosaur
call delay
push [bp+4]
call dinosaur_clear
jnz jump_up_twice
push [bp+4]
call dinosaur
push [bp+8]
push [bp+6]
call obs_proc
pop bx
pop dx
pop ax
pop bp
ret 6
endp dinosaur_up_procs
proc dinosaur_down_procs
push bp
mov bp,sp
push ax
push dx
push bx
mov ax,0
jump_down_twice:

push [bp+8]
push [bp+6]
call obs_proc
push [bp+4]
call dinosaur_clear
push [bp+4]
call check_jump_down
push [bp+4]
call dinosaur
call delay
jnz jump_down_twice
push [bp+4]
call dinosaur
push [bp+8]
push [bp+6]
call obs_proc
stop:
pop bx
pop dx
pop ax
pop bp
ret 6
endp dinosaur_down_procs


proc check_touch
push bp
mov bp,sp
push bx
push ax
push si
push cx
push dx
xor dx,dx
xor cx,cx
xor ax,ax
xor si,si
xor bx,bx
mov cx,20 ;the x of dinosaur
mov bx,[bp+4]
mov bx,[bx]
sub bx,20
mov ax,bx
mov bx,320
div bx
mov dx,ax
add dx,6
mov ah,0Dh
xor al,al
xor bh,bh
int 10h
mov dl,al
xor dh,dh
mov [bp+6],dx
pop dx
pop cx
pop si
pop ax
pop bx
pop bp
ret 2
endp check_touch
start:
	mov ax, @data
	mov ds, ax
; --------------------------
; Your code here
mov ax,13h
int 10h
mov ax,0a000h
mov es, ax
call clear
call under_line
push offset dinosaur_body
call dinosaur
push offset triangle_body
call triangle
push offset ribua_body
call ribua
call under_line

main_lop:
; mov dx,199
; mov cx,100
; mov ah,0Dh
; xor al,al
; xor bh,bh
; int 10h
	xor bx,bx
	xor cx,cx
	xor dx,dx
	xor si,si
	mov ah,0
	int 16h
	cmp al,'w'
	jz dinosaur_up_jump
	jmp main_lop

dinosaur_up_jump:
push offset ribua_body
push offset triangle_body
push offset dinosaur_body
call dinosaur_up_procs
add cx,1
cmp cx,20
jnz dinosaur_up_jump
xor cx,cx
dinosaur_down_jump:
push offset ribua_body
push offset triangle_body
push offset dinosaur_body
call dinosaur_down_procs
push dx
push offset dinosaur_body
call check_touch
pop dx
cmp dx,028h
jz fuck
add cx,1
cmp cx,20
jnz dinosaur_down_jump
fuck:
mov ah,1
	int 16h
	jz obs
jmp main_lop

obs:
push offset ribua_body
push offset triangle_body
call obs_proc_2
jmp fuck
; --------------------------

exit:
	mov ax, 4c00h
	int 21h
END start


